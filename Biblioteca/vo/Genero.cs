﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class Genero
    {
        public int id { set; get; }
        public string genero { set; get; }

        public Genero(int id, string genero)            
        {
            this.id = id;
            this.genero = genero;
        }
    }
}
