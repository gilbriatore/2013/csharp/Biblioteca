﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class Aluno : Pessoa
    {
        public string fone { set; get; }
        public string email { set; get; }

        public Aluno(int id, string nome, string fone, string email)
            : base(id, nome)
        {
            this.fone = fone;
            this.email = email;
        }
    }
}
