﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class ItemEmprestimo
    {
        public Livro livro { set; get; }
        public char situacao { set; get; }

        public ItemEmprestimo(Livro livro, char situacao)
        {
            this.livro = livro;
            this.situacao = situacao;
        }
    }
}
