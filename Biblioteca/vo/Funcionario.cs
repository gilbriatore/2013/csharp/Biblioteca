﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class Funcionario : Pessoa
    {
        public string fone { set; get; }
        public string email { set; get; }

        public Funcionario(int id, string nome, string fone, string email)
            : base(id, nome)
        {
            this.fone = fone;
            this.email = email;
        }
    }
}
