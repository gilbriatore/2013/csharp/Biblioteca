﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class Emprestimo
    {
        public int id { set; get; }
        public Aluno aluno { set; get; }
        public Funcionario funcionario { set; get; }
        public List<ItemEmprestimo> itens = new List<ItemEmprestimo>();
    }
}
