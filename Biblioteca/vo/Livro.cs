﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class Livro
    {
        public int id {set; get;}
        public string titulo {set; get;}
        public Autor autor {set; get;}
        public Genero genero { set; get; }

        public Livro(string titulo)
        {
            this.titulo = titulo;
        }

        public Livro(int id, string titulo, Autor autor, Genero genero)
        {
            this.id = id;
            this.titulo = titulo;
            this.autor = autor;
            this.genero = genero;
        }
    }
}
