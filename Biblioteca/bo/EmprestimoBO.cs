﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using vo;

namespace bo
{
    class EmprestimoBO
    {
        public static void adicionarLivro(Emprestimo emprestimo, ItemEmprestimo item)
        {
            emprestimo.itens.Add(item);
        }

        public static void devolverLivro(Emprestimo emprestimo, Livro livro)
        {
            foreach (ItemEmprestimo item in emprestimo.itens)
            {
                if(item.livro.titulo.Equals(livro.titulo))
                {
                    item.situacao = 'D';
                }
            }
        }

        public static void devolverTodos(Emprestimo emprestimo)
        {
            foreach (ItemEmprestimo item in emprestimo.itens)
            {
                item.situacao = 'D';
            }
        }

        public static int calcularEmprestados(Emprestimo emprestimo)
        {
            int i = 0;
            foreach (ItemEmprestimo item in emprestimo.itens)
            {
                if (item.situacao == 'E')
                {
                    i++;
                }
            }
            return i;
        }

        public static int calcularDevolvidos(Emprestimo emprestimo)
        {
            int i = 0;
            foreach (ItemEmprestimo item in emprestimo.itens)
            {
                if (item.situacao == 'D')
                {
                    i++;
                }
            }
            return i;
        }
    }
}
