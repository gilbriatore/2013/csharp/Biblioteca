﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using vo;
using bo;

namespace view
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Criação do aluno
            Aluno aluno = new Aluno(1, "José", "(11) 1111-1111", "111@111.com.br");
            //Criação do funcionário
            Funcionario funcionario = new Funcionario(1, "Mario", "(22) 2222-2222", "22@222.com.br");
            //Criação do emprestimo
            Emprestimo emprestimo = new Emprestimo();
            emprestimo.id = 1;
            emprestimo.aluno = aluno;
            emprestimo.funcionario = funcionario;
            //**************************************
            //Criação do autor do livro 1
            Autor autor;
            autor = new Autor(1, "Autor 1");
            //Criação do genero do livro 1
            Genero genero;
            genero = new Genero(1, "Genero 1");
            //Criação do primeiro livro do emprestimo
            Livro livro;
            livro = new Livro(1, "Titulo 01", autor, genero);
            //Criar o item do emprestimo
            ItemEmprestimo item;
            item = new ItemEmprestimo(livro, 'E');
            //Adicionar o livro ao emprestimo
            EmprestimoBO.adicionarLivro(emprestimo, item);
            //**************************************
            //Criação do autor do livro 2
            autor = new Autor(2, "Autor 2");
            //Criação do genero do livro 2
            genero = new Genero(2, "Genero 2");
            //Criação do segundo livro do emprestimo
            livro = new Livro(2, "Titulo 02", autor, genero);
            //Criar o item do emprestimo
            item = new ItemEmprestimo(livro, 'E');
            //Adicionar o livro ao emprestimo
            EmprestimoBO.adicionarLivro(emprestimo, item);
            //**************************************
            //Criação do autor do livro 3
            autor = new Autor(1, "Autor 1");
            //Criação do genero do livro 3
            genero = new Genero(2, "Genero 2");
            //Criação do terceiro livro do emprestimo
            livro = new Livro(3, "Titulo 03", autor, genero);
            //Criar o item do emprestimo
            item = new ItemEmprestimo(livro, 'E');
            //Adicionar o livro ao emprestimo
            EmprestimoBO.adicionarLivro(emprestimo, item);
            //**************************************
            //Mostrar os dados do emprestimo
            MessageBox.Show("ID: " + emprestimo.id + "\n" +
                            "Aluno: " + emprestimo.aluno.nome + "\n" +
                            "Funcionário: " + emprestimo.funcionario.nome);
            MessageBox.Show("Em seguida serão apresentados os livros do emprestimo");
            string situacao;
            foreach(ItemEmprestimo i in emprestimo.itens)
            {
                if(i.situacao == 'E')
                {
                    situacao = "EMPRESTADO";
                }
                else
                {
                    situacao = "DEVOLVIDO";
                }
                MessageBox.Show("Livro: " + i.livro.titulo + "\n" +
                                "Autor: " + i.livro.autor.nome + "\n" +
                                "Genero: " + i.livro.genero.genero + "\n" +
                                "Situação: " + situacao);
            }
            //Devolver o segundo livro do emprestimo
            livro = new Livro("Titulo 02");
            EmprestimoBO.devolverLivro(emprestimo, livro);
            //Mostrar os dados do emprestimo
            MessageBox.Show("ID: " + emprestimo.id + "\n" +
                            "Aluno: " + emprestimo.aluno.nome + "\n" +
                            "Funcionário: " + emprestimo.funcionario.nome);
            MessageBox.Show("Em seguida serão apresentados os livros do emprestimo");
            foreach (ItemEmprestimo i in emprestimo.itens)
            {
                if (i.situacao == 'E')
                {
                    situacao = "EMPRESTADO";
                }
                else
                {
                    situacao = "DEVOLVIDO";
                }
                MessageBox.Show("Livro: " + i.livro.titulo + "\n" +
                                "Autor: " + i.livro.autor.nome + "\n" +
                                "Genero: " + i.livro.genero.genero + "\n" +
                                "Situação: " + situacao);
            }
            //Mostrar a quantidade de livros emprestados
            MessageBox.Show("Quantidade de livros emprestados: " + EmprestimoBO.calcularEmprestados(emprestimo));
            //Mostrar a quantidade de livros devolvidos
            MessageBox.Show("Quantidade de livros devolvidos: " + EmprestimoBO.calcularDevolvidos(emprestimo));
            //Devolver todos os livros
            EmprestimoBO.devolverTodos(emprestimo);
            MessageBox.Show("Em seguida serão apresentados os livros do emprestimo");
            foreach (ItemEmprestimo i in emprestimo.itens)
            {
                if (i.situacao == 'E')
                {
                    situacao = "EMPRESTADO";
                }
                else
                {
                    situacao = "DEVOLVIDO";
                }
                MessageBox.Show("Livro: " + i.livro.titulo + "\n" +
                                "Autor: " + i.livro.autor.nome + "\n" +
                                "Genero: " + i.livro.genero.genero + "\n" +
                                "Situação: " + situacao);
            }
        }
    }
}
